package org.reisal78.ea.exceptions;

/**
 * Created by Simagin Igor on 05.01.2017.
 */
public class ValidationException extends RuntimeException {

    public ValidationException(String message) {
        super(message);
    }


}
