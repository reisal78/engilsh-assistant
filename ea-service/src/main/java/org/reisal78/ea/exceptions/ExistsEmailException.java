package org.reisal78.ea.exceptions;

/**
 * Created by Simagin Igor on 05.01.2017.
 */
public class ExistsEmailException extends Throwable {
    public ExistsEmailException(String message) {
        super(message);
    }
}
