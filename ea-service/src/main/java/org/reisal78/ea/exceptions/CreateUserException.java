package org.reisal78.ea.exceptions;

/**
 * Created by Simagin Igor on 05.01.2017.
 */
public class CreateUserException extends Throwable {
    public CreateUserException(String message) {
        super(message);
    }
}
