package org.reisal78.ea.exceptions;

/**
 * Created by Simagin Igor on 05.01.2017.
 */
public class ExistUsernameException extends Throwable {
    public ExistUsernameException(String message) {
        super(message);
    }
}
