package org.reisal78.ea.service;

import org.reisal78.ea.exceptions.CreateUserException;
import org.reisal78.ea.exceptions.ExistUsernameException;
import org.reisal78.ea.exceptions.ExistsEmailException;
import org.reisal78.ea.service.dto.NewUserDto;
import org.reisal78.ea.service.dto.UserDto;

import java.util.Locale;

/**
 * Created by Simagin Igor on 20.12.2016.
 */
public interface UserService {


    boolean authorize(String username, String password);

    UserDto getCurrentUser();

    void setUserLocale(Locale locale);

    void createNewUser(NewUserDto user) throws CreateUserException, ExistsEmailException, ExistUsernameException;

}
