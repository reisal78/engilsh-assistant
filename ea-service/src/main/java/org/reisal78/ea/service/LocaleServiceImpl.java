package org.reisal78.ea.service;

import org.reisal78.ea.domain.entity.Locales;
import org.reisal78.ea.domain.repositories.LocalesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
@Service
public class LocaleServiceImpl implements LocaleService {

    @Autowired
    private LocalesRepository localesRepository;

    @Override
    public Locale[] getLocales() {
        List<Locales> localesList = localesRepository.findAll();
        List<Locale> locales = new ArrayList<>();
        for (Locales l : localesList) {
            locales.add(l.getLocale());
        }
        return locales.toArray(new Locale[locales.size()]);
    }
}
