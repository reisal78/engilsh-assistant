package org.reisal78.ea.service;

import org.reisal78.ea.domain.entity.CustomUserDetails;
import org.reisal78.ea.domain.entity.User;
import org.reisal78.ea.domain.repositories.UserRepository;
import org.reisal78.ea.domain.repositories.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by Simagin Igor on 21.12.2016.
 */
@Service
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;
    private final UserRoleRepository userRoleRepository;

    @Autowired
    public CustomUserDetailsService(UserRepository userRepository, UserRoleRepository userRoleRepository) {
        this.userRepository = userRepository;
        this.userRoleRepository = userRoleRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username);
        if (null == user) {
            throw new UsernameNotFoundException("No user present with name: " + username);
        }else {
            List<String> userRoles = userRoleRepository.findRoleByUsername(username);
            return new CustomUserDetails(user, userRoles);
        }

    }
}
