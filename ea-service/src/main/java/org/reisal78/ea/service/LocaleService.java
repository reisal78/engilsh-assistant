package org.reisal78.ea.service;

import java.util.Locale;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
public interface LocaleService {
    Locale[] getLocales();
}
