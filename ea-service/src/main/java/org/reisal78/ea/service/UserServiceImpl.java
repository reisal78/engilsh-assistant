package org.reisal78.ea.service;

import org.reisal78.ea.domain.entity.AnonymousUser;
import org.reisal78.ea.domain.entity.User;
import org.reisal78.ea.domain.entity.UserRole;
import org.reisal78.ea.domain.repositories.AnonymousUserRepository;
import org.reisal78.ea.domain.repositories.UserRepository;
import org.reisal78.ea.domain.repositories.UserRoleRepository;
import org.reisal78.ea.exceptions.ExistUsernameException;
import org.reisal78.ea.exceptions.ExistsEmailException;
import org.reisal78.ea.exceptions.ValidationException;
import org.reisal78.ea.security.SecurityUtils;
import org.reisal78.ea.service.dto.DtoConverter;
import org.reisal78.ea.service.dto.NewUserDto;
import org.reisal78.ea.service.dto.UserDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.WebAuthenticationDetails;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.*;

/**
 * Created by Simagin Igor on 21.12.2016.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    protected AuthenticationManager authenticationManager;

    @Autowired
    private AnonymousUserRepository anonymousUserRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private ValidatorFactory validatorFactory;

    @Autowired
    private PasswordEncoder passwordEncoder;

    private Object lockUserCheck = new Object();


    @Override
    public boolean authorize(String username, String password) {
        try {
            Authentication token = authenticationManager
                    .authenticate(new UsernamePasswordAuthenticationToken(username, password));
            SecurityContextHolder.getContext().setAuthentication(token);
            return true;
        } catch (Exception e) {
            return false;
        }
    }


    @Override
    @Transactional
    public synchronized UserDto getCurrentUser() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (SecurityUtils.isLoggedIn()) {
            User registeredUser = userRepository.findByUsername(authentication.getName());
            registeredUser.setLastLogin(new Date());
            return DtoConverter.convertUserToUserDto(userRepository.save(registeredUser));
        } else {
            String sessionId = ((WebAuthenticationDetails) authentication.getDetails()).getSessionId();
            AnonymousUser anonymousUser = anonymousUserRepository.findBySession(sessionId);
            if (null == anonymousUser) {

                anonymousUser = anonymousUserRepository.save(new AnonymousUser(sessionId));
                anonymousUser.setDisplayedName("Anonymous#" + anonymousUser.getId());
            } else {
                anonymousUser.setLastLogin(new Date());
            }
            return DtoConverter.convertUserToUserDto(anonymousUserRepository.save(anonymousUser));
        }
    }


    @Override
    @Transactional
    public synchronized void setUserLocale(Locale locale) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (SecurityUtils.isLoggedIn()) {
            User user = userRepository.findByUsername(SecurityContextHolder.getContext().getAuthentication().getName());
            user.setLocale(locale);
            userRepository.save(user);
        } else {
            String sessionId = ((WebAuthenticationDetails) authentication.getDetails()).getSessionId();
            AnonymousUser user = anonymousUserRepository.findBySession(sessionId);
            user.setLocale(locale);
            anonymousUserRepository.save(user);
        }
    }

    @Override
    public void createNewUser(NewUserDto userDto) throws ExistsEmailException, ExistUsernameException {
        validateDto(userDto);
        User newUser = DtoConverter.convertNewUserDtoToUser(userDto);
        String password = passwordEncoder.encode(newUser.getPassword());
        newUser.setPassword(password);
        newUser.setEnabled(true);

        checkUser(newUser);

        User persistUser = userRepository.save(newUser);
        // TODO: 06.01.17 изменить присвоение роли
        UserRole userRole = new UserRole();
        userRole.setRole("USER");
        userRole.setUserId(persistUser.getId());
        userRoleRepository.save(userRole);
    }


    @Transactional
    private void checkUser(User newUser) throws ExistUsernameException, ExistsEmailException {
        if (null != userRepository.findByUsername(newUser.getUsername())) {
            throw new ExistUsernameException("Username already exists");
        }
        if (null != userRepository.findByEmail(newUser.getEmail())) {
            throw new ExistsEmailException("Email already exists");
        }
    }


    private <T> void validateDto(T dto) {
        Validator validator = validatorFactory.getValidator();
        Set<ConstraintViolation<T>> errors = validator
                .validate(dto);
        if (errors.size() > 0) {
            List<String> errorMessages = new ArrayList<>(errors.size());
            for (ConstraintViolation<T> cv : errors) {
                errorMessages.add(String.format("[Property: '%s', value '%s', %s]",
                        cv.getPropertyPath(), cv.getInvalidValue(), cv.getMessage()));
            }
            throw new ValidationException(Arrays.toString(errorMessages.toArray(new String[errorMessages.size()])));
        }
    }
}
