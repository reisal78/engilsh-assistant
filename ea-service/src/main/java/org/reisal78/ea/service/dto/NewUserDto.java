package org.reisal78.ea.service.dto;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by Simagin Igor on 04.01.2017.
 */
public class NewUserDto {

    @NotNull
    @NotEmpty
    @Size (min=3, max = 12)
    private String username;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 12)
    private String password;

    @NotNull
    @NotEmpty
    @Size(min = 3, max = 12)
    private String confirmPassword;

    @Email
    @NotEmpty
    private String email;

    public NewUserDto() {
    }


    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        this.confirmPassword = confirmPassword;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString() {
        return "NewUserDto{" +
                "username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", email='" + email + '\'' +
                '}';
    }
}
