package org.reisal78.ea.service;

import org.reisal78.ea.domain.repositories.AnonymousUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;

/**
 * Created by reisal78 on 21.12.16.
 */
@Service
public class AnonymousUserServiceImpl implements AnonymousUserService {

    @Autowired
    private AnonymousUserRepository anonymousUserRepository;

    public String getUsername() {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        System.out.println(authentication.getDetails().getClass());
        return "";
    }

}
