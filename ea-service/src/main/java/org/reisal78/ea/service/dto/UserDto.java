package org.reisal78.ea.service.dto;

import java.util.Locale;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
public class UserDto {
    private String displayedName;
    private Locale locale;

    public UserDto() {
    }

    public UserDto(String displayedName) {
        this.displayedName = displayedName;
    }

    public String getDisplayedName() {
        return displayedName;
    }

    public void setDisplayedName(String displayedName) {
        this.displayedName = displayedName;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
