package org.reisal78.ea.service.dto;

import org.reisal78.ea.domain.entity.AbstractUser;
import org.reisal78.ea.domain.entity.User;
import org.springframework.stereotype.Component;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
@Component
public class DtoConverter {



    public static UserDto convertUserToUserDto(AbstractUser user) {
        UserDto userDto = new UserDto();
        userDto.setDisplayedName(user.getDisplayedName());
        userDto.setLocale(user.getLocale());
        return userDto;
    }

    public static User convertNewUserDtoToUser(NewUserDto newUserDto) {
        User user = new User();
        user.setUsername(newUserDto.getUsername().toLowerCase());
        user.setPassword(newUserDto.getPassword());
        user.setDisplayedName(newUserDto.getUsername());
        user.setEmail(newUserDto.getEmail());
        return user;
    }
}
