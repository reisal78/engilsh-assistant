package org.reisal78.ea.ui.presenters;

import org.reisal78.ea.service.UserService;
import org.reisal78.ea.ui.LoginPresenter;
import org.reisal78.ea.ui.LoginView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Simagin Igor on 26.12.2016.
 */
@Component
public class LoginPresenterImpl implements LoginPresenter {

    @Autowired
    public UserService userService;
    private LoginView view;

    @Override
    public void setView(LoginView view) {
        this.view = view;
    }

    @Override
    public void authorizeEvent(String username, String password) {

        if (userService.authorize(username, password)) {
            view.navigateToRoot();
        } else {
            view.showAuthorizationErrorMessage();
        }


    }
}
