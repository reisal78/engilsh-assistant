package org.reisal78.ea.ui;

/**
 * Created by Simagin Igor on 05.01.2017.
 */
public interface LocalizeConstants {
    String BAD_CREDENTIALS_MESSAGE = "message.bad_credential";
    String GREETING_LABEL_VALUE = "greeting_panel.greeting";
    String REGISTER_LINK_CAPTION = "login_link_panel.register_link.caption";
    String RESTORE_PASSWORD_LINK_CAPTION = "login_link_panel.restore_password_linl.caption";
    String LOGIN_BUTTON_CAPTION = "login_panel.login_button.caption";

    String USERNAME_FIELD_CAPTION = "register_form.username_field.caption";
    String PASSWORD_FIELD_CAPTION = "register_form.password_field.caption";
    String CONFIRM_PASSWORD_FIELD_CAPTION = "register_form.confirm_password_field.caption";
    String EMAIL_FIELD_CAPTION = "register_form.email_field.caption";
    String REGISTER_BUTTON_CAPTION = "register_form.register_button.caption";
    String REGISTER_FORM_CAPTION = "register_form.caption";

    String REQUIRED_FIELD_MESSAGE = "message.is_required";
    String ERROR_LENGTH_MESSAGE = "message.error_length";
    String ERROR_EMAIL_MESSAGE = "message.error_email";

    String  PASSWORDS_NOT_MATCH_MESSAGE = "message.passwords_not_match";
}
