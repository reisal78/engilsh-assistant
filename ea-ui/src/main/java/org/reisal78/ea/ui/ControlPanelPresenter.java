package org.reisal78.ea.ui;

import java.util.Locale;

/**
 * Created by Simagin Igor on 26.12.2016.
 */
public interface ControlPanelPresenter extends GeneralPresenter <ControlPanelView> {
    String getUserName();

    Locale[] getLocales();

    void setUserLocale(Locale locale);

    Locale getUserLocale();

}
