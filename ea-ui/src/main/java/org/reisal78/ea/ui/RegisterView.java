package org.reisal78.ea.ui;

/**
 * Created by Simagin Igor on 02.01.2017.
 */
public interface RegisterView extends GeneralView<RegisterPresenter>{

    String getUsername();


    String getPassword();


    String getConfirmPassword();


    String getEmail();

    void setUsername(String username);

    void setPassword(String password);

    void setConfirmPassword(String confirmPassword);

    void setEmail(String email);

    void showUsernameError(String error);

    void showPasswordError(String error);

    void showConfirmPasswordError(String error);

    void showEmailError(String error);

    void navigateToRoot();
}
