package org.reisal78.ea.ui.presenters;

import org.reisal78.ea.exceptions.CreateUserException;
import org.reisal78.ea.exceptions.ExistUsernameException;
import org.reisal78.ea.exceptions.ExistsEmailException;
import org.reisal78.ea.service.UserService;
import org.reisal78.ea.service.dto.NewUserDto;
import org.reisal78.ea.ui.LocalizeConstants;
import org.reisal78.ea.ui.RegisterPresenter;
import org.reisal78.ea.ui.RegisterView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by Simagin Igor on 02.01.2017.
 */
@Component
public class RegisterPresenterImpl implements RegisterPresenter, LocalizeConstants {

    private RegisterView view;

    @Autowired
    private UserService userService;

    @Override
    public void setView(RegisterView view) {
        this.view = view;
    }

    @Override
    public void registerEvent() {
        NewUserDto userDto = new NewUserDto();
        userDto.setUsername(view.getUsername());
        userDto.setPassword(view.getPassword());
        userDto.setConfirmPassword(view.getConfirmPassword());
        userDto.setEmail(view.getEmail());
        try {
            userService.createNewUser(userDto);
            view.navigateToRoot();
        } catch (CreateUserException e) {
            System.out.println("unknown error");
        } catch (ExistsEmailException e) {
            view.showEmailError("Email already exists");
        } catch (ExistUsernameException e) {
            view.showUsernameError("Username already exists");
        }
    }
}
