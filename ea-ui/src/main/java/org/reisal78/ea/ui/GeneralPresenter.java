package org.reisal78.ea.ui;

/**
 * Created by Simagin Igor on 21.12.2016.
 */
public interface GeneralPresenter <T extends GeneralView>{
    void setView(T view);
}
