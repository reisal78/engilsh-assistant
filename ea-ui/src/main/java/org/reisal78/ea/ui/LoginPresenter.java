package org.reisal78.ea.ui;

/**
 * Created by Simagin Igor on 26.12.2016.
 */
public interface LoginPresenter extends GeneralPresenter<LoginView> {

    void authorizeEvent(String username, String password);
}
