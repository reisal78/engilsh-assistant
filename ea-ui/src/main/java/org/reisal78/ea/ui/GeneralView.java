package org.reisal78.ea.ui;

/**
 * Created by Simagin Igor on 21.12.2016.
 */
public interface GeneralView <T extends GeneralPresenter> {
    void setPresenter(T presenter);
}
