package org.reisal78.ea.ui.presenters;

import org.reisal78.ea.service.LocaleService;
import org.reisal78.ea.service.UserService;
import org.reisal78.ea.ui.ControlPanelPresenter;
import org.reisal78.ea.ui.ControlPanelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Locale;

/**
 * Created by Simagin Igor on 26.12.2016.
 */
@Component
public class ControlPanelPresenterImpl implements ControlPanelPresenter {

    private ControlPanelView view;

    @Autowired
    private UserService userService;

    @Autowired
    private LocaleService localeService;

    @Override
    public void setView(ControlPanelView view) {
        this.view = view;
    }

    @Override
    public String getUserName() {
        return userService.getCurrentUser().getDisplayedName();
    }

    @Override
    public Locale[] getLocales() {
        return localeService.getLocales();
    }

    @Override
    public void setUserLocale(Locale locale) {
        userService.setUserLocale(locale);
        view.reload();
    }

    @Override
    public Locale getUserLocale() {
        return userService.getCurrentUser().getLocale();
    }
}
