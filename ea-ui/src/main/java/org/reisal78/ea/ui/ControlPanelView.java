package org.reisal78.ea.ui;

/**
 * Created by Simagin Igor on 26.12.2016.
 */
public interface ControlPanelView extends GeneralView<ControlPanelPresenter> {

    void setUserName(String userName);

    void reload();

}
