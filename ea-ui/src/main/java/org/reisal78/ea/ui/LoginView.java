package org.reisal78.ea.ui;

/**
 * Created by Simagin Igor on 26.12.2016.
 */
public interface LoginView extends GeneralView <LoginPresenter> {
    void navigateToRoot();
    void showAuthorizationErrorMessage();
}
