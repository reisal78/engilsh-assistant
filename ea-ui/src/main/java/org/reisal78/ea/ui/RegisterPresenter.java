package org.reisal78.ea.ui;

/**
 * Created by Simagin Igor on 02.01.2017.
 */
public interface RegisterPresenter extends GeneralPresenter<RegisterView> {
    void registerEvent();
}
