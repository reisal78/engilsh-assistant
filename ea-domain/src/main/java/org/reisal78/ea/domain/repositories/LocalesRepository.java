package org.reisal78.ea.domain.repositories;

import org.reisal78.ea.domain.entity.Locales;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
public interface LocalesRepository extends JpaRepository<Locales, Long> {

}
