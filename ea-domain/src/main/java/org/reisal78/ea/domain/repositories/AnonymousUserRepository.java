package org.reisal78.ea.domain.repositories;

import org.reisal78.ea.domain.entity.AnonymousUser;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by reisal78 on 21.12.16.
 */
public interface AnonymousUserRepository extends JpaRepository<AnonymousUser, Long> {
    AnonymousUser findBySession(String session);
}
