package org.reisal78.ea.domain.entity;

import javax.persistence.Entity;
import java.util.Locale;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
@Entity
public class Locales extends AbstractEntity {
    private Locale locale;

    public Locales(Locale locale) {
        this.locale = locale;
    }

    public Locales() {
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }
}
