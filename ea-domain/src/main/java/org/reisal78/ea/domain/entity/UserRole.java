package org.reisal78.ea.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by Simagin Igor on 21.12.2016.
 */
@Entity
@Table(name = "user_role")
public class UserRole extends AbstractEntity {

    private String role;
    private Long userId;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public Long getUserId() {
        return userId;
    }

    public void setUserId(Long userId) {
        this.userId = userId;
    }
}
