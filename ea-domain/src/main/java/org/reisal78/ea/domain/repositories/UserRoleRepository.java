package org.reisal78.ea.domain.repositories;

import org.reisal78.ea.domain.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Created by Simagin Igor on 21.12.2016.
 */
public interface UserRoleRepository extends JpaRepository<UserRole, Long> {

    @Query("select a.role from UserRole a, User b where b.username=?1 and a.userId=b.id")
    List<String> findRoleByUsername(String username);
}
