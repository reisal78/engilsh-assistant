package org.reisal78.ea.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;
import java.util.Locale;

/**
 * Created by reisal78 on 21.12.16.
 */
@Entity
@Table(name = "anonymous_users")
public class AnonymousUser extends AbstractUser {

    private String session;



    public AnonymousUser() {

    }

    public AnonymousUser(String sessionId) {
        this.session = sessionId;
    }

    public AnonymousUser(String sessionId, Locale english) {
        this(sessionId);
        setLocale(english);
    }

    public String getSession() {
        return session;
    }

    public void setSession(String session) {
        this.session = session;
    }




}
