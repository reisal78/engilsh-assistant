package org.reisal78.ea.domain.entity;

import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Locale;

/**
 * Created by reisal78 on 21.12.16.
 */
@Entity
@Table(name = "users")
public class User extends AbstractUser {

    public User() {
    }

    public User(User user) {
        this.username = user.getUsername();
        this.password = user.getPassword();
        this.email = user.getEmail();
        this.enabled = user.isEnabled();
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
