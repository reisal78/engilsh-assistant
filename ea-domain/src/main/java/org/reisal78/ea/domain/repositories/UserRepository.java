package org.reisal78.ea.domain.repositories;

import org.reisal78.ea.domain.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by Simagin Igor on 21.12.2016.
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);

    User findByEmail(String email);
}
