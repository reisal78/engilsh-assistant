package org.reisal78.ea.domain.entity;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import java.util.Date;
import java.util.Locale;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
@MappedSuperclass
public class AbstractUser extends AbstractEntity {

    @Column(unique = true)
    protected String username;
    protected String password;
    @Column(unique = true)
    protected String email;
    protected boolean enabled;
    protected Locale locale = Locale.ENGLISH;
    protected Date lastLogin = new Date();
    protected String displayedName;

    public AbstractUser() {
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

    public Locale getLocale() {
        return locale;
    }

    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    public Date getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(Date lastLogin) {
        this.lastLogin = lastLogin;
    }

    public String getDisplayedName() {
        return displayedName;
    }

    public void setDisplayedName(String displayedName) {
        this.displayedName = displayedName;
    }

    @Override
    public String toString() {
        return
                "AbstractUser{" +
                        "id='" + getId() + '\'' +
                        "username='" + username + '\'' +
                        ", password='" + password + '\'' +
                        ", email='" + email + '\'' +
                        ", enabled=" + enabled +
                        ", locale=" + locale +
                        ", lastLogin=" + lastLogin +
                        ", displayedName='" + displayedName + '\'' +
                        '}';
    }
}
