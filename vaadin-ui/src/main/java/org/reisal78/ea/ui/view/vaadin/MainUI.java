package org.reisal78.ea.ui.view.vaadin;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.Title;
import com.vaadin.navigator.Navigator;
import com.vaadin.navigator.ViewDisplay;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinSession;
import com.vaadin.spring.annotation.SpringUI;
import com.vaadin.spring.navigator.SpringViewProvider;
import com.vaadin.ui.*;

import org.reisal78.ea.service.UserService;
import org.reisal78.ea.service.dto.UserDto;
import org.reisal78.ea.ui.view.vaadin.views.AccessDeniedView;
import org.reisal78.ea.ui.view.vaadin.views.VaadinControlPanelView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;


@SpringUI(path = "/")
@Theme("mytheme")
@Title("English assistant")
public class MainUI extends UI {

    @Autowired
    AuthenticationManager authenticationManager;

    @Autowired
    private SpringViewProvider viewProvider;

    @Autowired
    private VaadinControlPanelView controlPanel;

    @Autowired
    public MainUI(UserService userService) {
        VaadinSession.getCurrent().setLocale(userService.getCurrentUser().getLocale());
    }

    @Override
    protected void init(VaadinRequest vaadinRequest) {

        Component viewContainer = createViewContainer();

        Navigator navigator = new Navigator(this, (ComponentContainer) viewContainer);
        navigator.addProvider(viewProvider);
        viewProvider.setAccessDeniedViewClass(AccessDeniedView.class);

        Component rootFrame = createRootFrame(viewContainer, controlPanel);

        setContent(rootFrame);
    }

    private Component createRootFrame(Component viewContainer, VaadinControlPanelView controlPanel) {

        final HorizontalLayout leftPanel = new HorizontalLayout();
        leftPanel.setSizeFull();
        leftPanel.addComponent(controlPanel);
        leftPanel.setComponentAlignment(controlPanel, Alignment.TOP_LEFT);

        final HorizontalLayout rightPanel = new HorizontalLayout();
        rightPanel.setSizeFull();
        rightPanel.addComponent(viewContainer);
        rightPanel.setComponentAlignment(viewContainer, Alignment.MIDDLE_CENTER);

        final HorizontalSplitPanel splitPanel = new HorizontalSplitPanel();
        splitPanel.setSplitPosition(20F);
        splitPanel.setSizeFull();
        splitPanel.setFirstComponent(leftPanel);
        splitPanel.setSecondComponent(rightPanel);

        final HorizontalLayout root = new HorizontalLayout();
        root.setSizeFull();
        root.setMargin(true);
        root.addComponent(splitPanel);

        return root;
    }

    private Component createViewContainer() {
        final HorizontalLayout viewContainer = new HorizontalLayout();
        viewContainer.setSizeUndefined();
        return viewContainer;
    }
}
