package org.reisal78.ea.ui.view.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.VerticalLayout;
import org.reisal78.ea.ui.RegisterPresenter;
import org.reisal78.ea.ui.RegisterView;
import org.reisal78.ea.ui.view.vaadin.components.RegisterForm;
import org.reisal78.ea.ui.view.vaadin.components.VaadinAbstractComponent;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * Created by Simagin Igor on 02.01.2017.
 */
@SpringView(name = VaadinRegisterView.VIEW_NAME)
@UIScope
public class VaadinRegisterView extends VaadinAbstractComponent implements View, RegisterView {
    public static final String VIEW_NAME = "register";
    private static final String DEFAULT_WIGHT = "300px";

    @Autowired
    private RegisterPresenter presenter;

    @Autowired
    private RegisterForm registerForm;

    @PostConstruct
    void init() {
        setWidth(DEFAULT_WIGHT);
        presenter.setView(this);

        registerForm.setRegisterListener(event -> {
            if (registerForm.validateAllFields()) {
                presenter.registerEvent();
            }
        });

        VerticalLayout root = new VerticalLayout();
        root.addComponent((Component) registerForm);

        setCompositionRoot(root);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent event) {
    }

    @Override
    public String getUsername() {
        return registerForm.getUsername();
    }

    @Override
    public void setUsername(String username) {
        registerForm.setUserName(username);
    }

    @Override
    public String getPassword() {
        return registerForm.getPassword();
    }

    @Override
    public void setPassword(String password) {
        registerForm.setPassword(password);
    }

    @Override
    public String getConfirmPassword() {
        return registerForm.getConfirmPassword();
    }

    @Override
    public void setConfirmPassword(String confirmPassword) {
        registerForm.setConfirmPassword(confirmPassword);
    }

    @Override
    public String getEmail() {
        return registerForm.getEmail();
    }

    @Override
    public void setEmail(String email) {
        registerForm.setEmail(email);
    }

    @Override
    public void showUsernameError(String error) {
        registerForm.showUsernameError(error);
    }

    @Override
    public void showPasswordError(String error) {
        registerForm.showPasswordError(error);
    }

    @Override
    public void showConfirmPasswordError(String error) {
        registerForm.showConfirmPasswordError(error);
    }

    @Override
    public void showEmailError(String error) {
        registerForm.showEmailError(error);
    }

    @Override
    public void navigateToRoot() {
        getUI().getNavigator().navigateTo(MainView.VIEW_NAME);
    }

    @Override
    public void setPresenter(RegisterPresenter presenter) {
        this.presenter = presenter;
    }
}
