package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.server.ExternalResource;
import com.vaadin.ui.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Simagin Igor on 02.01.2017.
 */
@Component
@Scope("prototype")
public class VaadinLoginLinkPanel extends VaadinAbstractComponent implements LoginLinkPanel {

    private Link registerLink;
    private Link restorePasswordLink;

    @PostConstruct
    void init() {

        registerLink = new Link();
        registerLink.setCaption(getLocalizedProperty(REGISTER_LINK_CAPTION));

        restorePasswordLink = new Link();
        restorePasswordLink.setCaption(getLocalizedProperty(RESTORE_PASSWORD_LINK_CAPTION));

        HorizontalLayout root = new HorizontalLayout();
        root.setSizeFull();
        root.setMargin(true);
        root.addComponent(restorePasswordLink);
        root.setComponentAlignment(restorePasswordLink, Alignment.TOP_LEFT);
        root.addComponent(registerLink);
        root.setComponentAlignment(registerLink, Alignment.TOP_RIGHT);


        Panel registerPanel = new Panel();
        registerPanel.setSizeFull();
        registerPanel.setContent(root);

        setCompositionRoot(registerPanel);
    }

    @Override
    public void setRegisterPath(String path) {
        registerLink.setResource(new ExternalResource(path));
    }

    @Override
    public void setRestorePasswordPath(String path) {
        restorePasswordLink.setResource(new ExternalResource(path));
    }
}
