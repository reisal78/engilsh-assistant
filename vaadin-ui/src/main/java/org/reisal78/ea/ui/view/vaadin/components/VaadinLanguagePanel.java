package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.ui.Button;
import com.vaadin.ui.HorizontalLayout;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
@Component
@Scope ("prototype")
public class VaadinLanguagePanel extends VaadinAbstractComponent implements LanguagePanel {

    private HorizontalLayout root;


    private List<Button> langButtons = new ArrayList<>();



    @PostConstruct
    void init() {

        root = new HorizontalLayout();
        root.setSpacing(true);
        setCompositionRoot(root);
    }

    @Override
    public void addLocaleButton(Button button) {
        langButtons.add(button);
        root.addComponent(button);
    }

    @Override
    public void disableCurrentLocaleButton() {

    }
}
