package org.reisal78.ea.ui.view.vaadin.components;

/**
 * Created by Simagin Igor on 02.01.2017.
 */
public interface LoginLinkPanel {


    void setRegisterPath(String path);

    void setRestorePasswordPath(String path);
}
