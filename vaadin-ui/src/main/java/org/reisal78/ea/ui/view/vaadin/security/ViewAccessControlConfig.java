package org.reisal78.ea.ui.view.vaadin.security;

import com.vaadin.spring.access.ViewAccessControl;
import com.vaadin.ui.UI;
import org.reisal78.ea.security.SecurityUtils;
import org.springframework.stereotype.Component;

/**
 * Created by Simagin Igor on 20.12.2016.
 */
@Component
public class ViewAccessControlConfig implements ViewAccessControl {
    @Override
    public boolean isAccessGranted(UI ui, String beanName) {

        switch (beanName) {
            case "adminView":
                return SecurityUtils.hasRole("ADMIN");
            case "vaadinLoginView":
                return !SecurityUtils.isLoggedIn();
            default:
                return true;
        }
    }
}
