package org.reisal78.ea.ui.view.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Simagin Igor on 18.12.2016.
 */

@Component
@Scope("prototype")
public class AccessDeniedView extends CustomComponent implements View {


    public static final String VIEW_NAME = "access_denied";

    @PostConstruct
    void init() {
        setCompositionRoot(new VerticalLayout(new Label("Access Denied")));
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
