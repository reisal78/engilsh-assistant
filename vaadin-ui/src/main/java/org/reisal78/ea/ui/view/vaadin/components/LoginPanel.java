package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.ui.Button;

/**
 * Created by Simagin Igor on 25.12.2016.
 */
public interface LoginPanel {




    void setLoginListener(Button.ClickListener listener);

    String getUsername();

    String getPassword();

    void clearPasswordField();
}
