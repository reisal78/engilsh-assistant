package org.reisal78.ea.ui.view.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.Component;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.VerticalLayout;
import org.reisal78.ea.ui.LoginPresenter;
import org.reisal78.ea.ui.LoginView;
import org.reisal78.ea.ui.view.vaadin.components.ErrorMessagePanel;
import org.reisal78.ea.ui.view.vaadin.components.LoginPanel;
import org.reisal78.ea.ui.view.vaadin.components.LoginLinkPanel;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;

/**
 * Created by Simagin Igor on 19.12.2016.
 */
@SpringView (name = VaadinLoginView.VIEW_NAME)
@UIScope
public class VaadinLoginView extends CustomComponent implements View, LoginView {

    public final static String DEFAULT_WIGHT = "300px";

    public static final String VIEW_NAME = "signin";

    @Autowired
    private ErrorMessagePanel errorMessagePanel;

    @Autowired
    private LoginPanel loginPanel;

    @Autowired
    private LoginPresenter presenter;

    @Autowired
    private LoginLinkPanel loginLinkPanel;

    @PostConstruct
    void init() {

        presenter.setView(this);

        setWidth(DEFAULT_WIGHT);

        loginPanel.setLoginListener(event -> {
            presenter.authorizeEvent(loginPanel.getUsername(), loginPanel.getPassword());
        });

        loginLinkPanel.setRegisterPath("/#!" + VaadinRegisterView.VIEW_NAME);

        VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        root.setSpacing(true);
        root.addComponent((Component) errorMessagePanel);
        root.addComponent((Component) loginPanel);
        root.addComponent((Component) loginLinkPanel);
        setCompositionRoot(root);
    }

    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }

    @Override
    public void setPresenter(LoginPresenter presenter) {
        this.presenter = presenter;
    }

    @Override
    public void navigateToRoot() {
        getUI().getPage().reload();
        getUI().getNavigator().navigateTo(MainView.VIEW_NAME);
    }

    @Override
    public void showAuthorizationErrorMessage() {
        loginPanel.clearPasswordField();
        errorMessagePanel.showAuthorizeErrorMessage();
    }
}
