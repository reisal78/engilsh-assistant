package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.data.Validator;
import com.vaadin.data.validator.EmailValidator;
import com.vaadin.data.validator.StringLengthValidator;
import com.vaadin.server.UserError;
import com.vaadin.ui.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Simagin Igor on 03.01.2017.
 */
@Component
@Scope("prototype")
public class VaadinRegisterForm extends VaadinAbstractComponent implements RegisterForm {



    private TextField userNameField;
    private PasswordField passwordField;
    private PasswordField confirmPasswordField;
    private TextField emailField;
    private Button registerButton;


    @PostConstruct
    void init() {

        addStyleName("registration-form");

        //Initialize fields
        userNameField = createField(new TextField(), USERNAME_FIELD_CAPTION);
        passwordField = createField(new PasswordField(), PASSWORD_FIELD_CAPTION);
        confirmPasswordField = createField(new PasswordField(), CONFIRM_PASSWORD_FIELD_CAPTION);
        emailField = createField(new TextField(), EMAIL_FIELD_CAPTION);
        registerButton = new Button(getLocalizedProperty(REGISTER_BUTTON_CAPTION));

        //Add validators
        userNameField.addValidator(
                new StringLengthValidator(getLocalizedProperty(ERROR_LENGTH_MESSAGE), 3, 12, false)
        );
        passwordField.addValidator(
                new StringLengthValidator(getLocalizedProperty(ERROR_LENGTH_MESSAGE), 3, 12, false)
        );
        confirmPasswordField.addValidator(
                new StringLengthValidator(getLocalizedProperty(ERROR_LENGTH_MESSAGE), 3, 12, false)
        );
        emailField.addValidator(
                new EmailValidator(getLocalizedProperty(ERROR_EMAIL_MESSAGE))
        );

        // TODO: 05.01.2017 Удалить значения для отладки
        userNameField.setValue("Alladin");
        passwordField.setValue("123");
        confirmPasswordField.setValue("123");
        emailField.setValue("igor.78@live.ru");

        //Add elements settings
        registerButton.setSizeFull();


        VerticalLayout root = new VerticalLayout();
        root.setSizeFull();
        root.setMargin(true);
        root.setSpacing(true);
        root.addComponent(userNameField);
        root.addComponent(passwordField);
        root.addComponent(confirmPasswordField);
        root.addComponent(emailField);
        root.addComponent(registerButton);

        Panel registerForm = new Panel();
        registerForm.setCaption(getLocalizedProperty(REGISTER_FORM_CAPTION));
        registerForm.setContent(root);
        setCompositionRoot(registerForm);
    }

    private <T extends AbstractTextField> T createField(T field, String caption) {
        field.setCaption(getLocalizedProperty(caption));
        //    VaadinUtils.setPlaceholder(field, getLocalizedProperty(caption));
        field.setRequired(true);
        field.setRequiredError(getLocalizedProperty(REQUIRED_FIELD_MESSAGE));
        field.setSizeFull();
        field.setValidationVisible(false);
        field.setNullRepresentation("");
        return field;
    }


    @Override
    public String getUsername() {
        return userNameField.getValue();
    }

    @Override
    public String getPassword() {
        return passwordField.getValue();
    }

    @Override
    public String getConfirmPassword() {
        return confirmPasswordField.getValue();
    }

    @Override
    public String getEmail() {
        return emailField.getValue();
    }

    @Override
    public void setUserName(String value) {
        userNameField.setValue(value);
    }

    @Override
    public void setPassword(String value) {
        passwordField.setValue(value);
    }

    @Override
    public void setConfirmPassword(String value) {
        confirmPasswordField.setValue(value);
    }

    @Override
    public void setEmail(String value) {
        emailField.setValue(value);
    }

    @Override
    public void setRegisterListener(Button.ClickListener listener) {
        registerButton.addClickListener(listener);
    }

    @Override
    public void showUsernameError(String error) {
        userNameField.setComponentError(new UserError(error));
    }

    @Override
    public void showPasswordError(String error) {
        passwordField.setComponentError(new UserError(error));
    }

    @Override
    public void showConfirmPasswordError(String error) {
        confirmPasswordField.setComponentError(new UserError(error));
    }

    @Override
    public void showEmailError(String error) {
        emailField.setComponentError(new UserError(error));
    }

    @Override
    public void clearAllErrors() {
        userNameField.setComponentError(null);
        passwordField.setComponentError(null);
        confirmPasswordField.setComponentError(null);
        emailField.setComponentError(null);
    }

    @Override
    public boolean validateAllFields() {
        if (!passwordField.getValue().equals(confirmPasswordField.getValue())) {
            confirmPasswordField.setComponentError(
                    new UserError(getLocalizedProperty(PASSWORDS_NOT_MATCH_MESSAGE))
            );
            return false;
        }
        return (
                        validateTextField(userNameField) &
                        validateTextField(passwordField) &
                        validateTextField(confirmPasswordField) &
                        validateTextField(emailField)
        );
    }

    private boolean validateTextField(AbstractTextField textField) {
        try {
            textField.validate();
            textField.setComponentError(null);
            return true;
        } catch (Validator.InvalidValueException e) {
            textField.setComponentError(new UserError(e.getMessage()));
            return false;
        }
    }

}
