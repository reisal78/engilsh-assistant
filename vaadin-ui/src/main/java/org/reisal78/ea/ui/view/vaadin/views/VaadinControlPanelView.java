package org.reisal78.ea.ui.view.vaadin.views;

import com.vaadin.server.ExternalResource;
import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.Link;
import com.vaadin.ui.VerticalLayout;
import org.reisal78.ea.ui.ControlPanelPresenter;
import org.reisal78.ea.ui.ControlPanelView;
import org.reisal78.ea.ui.view.vaadin.components.GreetingPanel;
import org.reisal78.ea.ui.view.vaadin.components.LanguagePanel;
import org.reisal78.ea.ui.view.vaadin.components.VaadinAbstractComponent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.Locale;

/**
 * Created by Simagin Igor on 22.12.2016.
 */
@Component
@Scope ("prototype")
public class VaadinControlPanelView extends VaadinAbstractComponent implements ControlPanelView{

    @Autowired
    private GreetingPanel greetingPanel;

    @Autowired
    private LanguagePanel languagePanel;

    @Autowired
    private ControlPanelPresenter presenter;


    @PostConstruct
    void init() {
        final VerticalLayout root = new VerticalLayout();
        presenter.setView(this);

        Locale[] locales = presenter.getLocales();

        for (Locale l : locales) {
            languagePanel.addLocaleButton(createLocaleButton(l));
        }


        root.addComponent((com.vaadin.ui.Component) languagePanel);
        root.addComponent((com.vaadin.ui.Component) greetingPanel);
        root.addComponent(new Link("Admin panel", new ExternalResource("/#!admin")));
        root.addComponent(new Link("Sign In", new ExternalResource("/#!signin")));
        root.addComponent(new Link("Logout", new ExternalResource("/logout")));
        root.setSpacing(true);
        setCompositionRoot(root);
    }

    private Button createLocaleButton(Locale locale) {
        Button button = new Button(locale.getLanguage());
        if (locale.equals(VaadinSession.getCurrent().getLocale())) {
            button.setEnabled(false);
        }
        button.addClickListener(event -> {
            presenter.setUserLocale(locale);
        });
        return button;
    }


    @Override
    public void setPresenter(ControlPanelPresenter presenter) {
        this.presenter = presenter;
    }


    @Override
    public void setUserName(String userName) {
        greetingPanel.setUsername(userName);
    }

    @Override
    public void reload() {
        getUI().getPage().reload();
    }


}
