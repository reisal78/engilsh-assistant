package org.reisal78.ea.ui.view.vaadin.components;

/**
 * Элемент пользовательского интерфейса, отображающий сообщения об ошибках
 * Created by reisal78 on 25.12.16.
 */
public interface ErrorMessagePanel {



    /**
     * Отображает сообщение об ошибке
     * @param message сообщение об ошибке
     */
    void showMessage(String message);

    /**
     * Убирает (закрывает) элемент отображающий сообщение об ошибке
     */
    void close();

    void showAuthorizeErrorMessage();
}
