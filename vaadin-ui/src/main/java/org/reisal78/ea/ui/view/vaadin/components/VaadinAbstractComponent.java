package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.server.VaadinSession;
import com.vaadin.ui.Button;
import com.vaadin.ui.CustomComponent;
import org.reisal78.ea.ui.LocalizeConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;

import java.util.Locale;

/**
 * Created by Simagin Igor on 25.12.2016.
 */
public abstract class VaadinAbstractComponent extends CustomComponent implements LocalizeConstants {

    @Autowired
    private MessageSource messageSource;

    protected Locale locale = VaadinSession.getCurrent().getLocale();



    @Override
    public Locale getLocale() {
        return locale;
    }

    @Override
    public void setLocale(Locale locale) {
        this.locale = locale;
    }

    protected String getLocalizedProperty(String key) {
        return messageSource.getMessage(key, null, locale);
    }


}
