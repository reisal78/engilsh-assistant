package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Элемент, отображающий сообщения об ошибках
 * Created by Simagin Igor on 23.12.2016.
 */
@Component
@Scope("prototype")
public class VaadinErrorMessagePanel extends VaadinAbstractComponent implements ErrorMessagePanel {

    private Label errorLabel = new Label();
    private Button closePanelButton = new Button();

    @PostConstruct
    void init() {

        addStyleName("error-message-panel"); //Настройка внешненго вида в файле scss


        closePanelButton.setIcon(FontAwesome.CLOSE);
        closePanelButton.addClickListener(e ->{
            close(); //Скрыть панель при нажатии на кнопку
        });

        setVisible(false);
        setSizeFull();

        final HorizontalLayout root = new HorizontalLayout();
        root.setMargin(true);
        root.setSpacing(true);
        root.addComponent(errorLabel);
        root.setComponentAlignment(errorLabel, Alignment.MIDDLE_LEFT);
        root.setExpandRatio(errorLabel, 80F);
        root.addComponent(closePanelButton);
        root.setComponentAlignment(closePanelButton, Alignment.MIDDLE_RIGHT);
        root.setExpandRatio(closePanelButton, 20F);

        root.setSizeFull();
        final Panel errorPanel = new Panel();

        errorPanel.setContent(root);
        setCompositionRoot(errorPanel);
    }


    /**
     * Отображает сообщение об ошибке
     *
     * @param message сообщение об ошибке
     */
    @Override
    public void showMessage(String message) {
        errorLabel.setValue(message);
        setVisible(true);
    }

    /**
     * Убирает (закрывает) элемент отображающий сообщение об ошибке
     */
    @Override
    public void close() {
        errorLabel.setValue("");
        setVisible(false);
    }

    @Override
    public void showAuthorizeErrorMessage() {
        showMessage(getLocalizedProperty(BAD_CREDENTIALS_MESSAGE));
    }
}
