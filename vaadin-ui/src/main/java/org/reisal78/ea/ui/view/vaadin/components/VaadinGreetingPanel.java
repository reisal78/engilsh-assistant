package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Label;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Simagin Igor on 21.12.2016.
 */
@Component
@Scope ("prototype")
public class VaadinGreetingPanel extends VaadinAbstractComponent implements GreetingPanel {

    private Label usernameLabel;

    @PostConstruct
    void init() {
        usernameLabel = new Label();
        final HorizontalLayout root = new HorizontalLayout();
        root.setSpacing(true);
        root.addComponent(new Label(getLocalizedProperty(GREETING_LABEL_VALUE)));
        root.addComponent(usernameLabel);
        setCompositionRoot(root);
    }



    @Override
    public void setUsername(String username) {
         usernameLabel.setValue(username);
    }
}
