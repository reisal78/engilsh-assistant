package org.reisal78.ea.ui.view.vaadin.utils;

import com.vaadin.data.Validator;

/**
 * Created by Simagin Igor on 05.01.2017.
 */
public class EmailValidator implements Validator {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";


    @Override
    public void validate(Object value) throws InvalidValueException {
        if (value instanceof String) {
            String email = (String)value;
            if (!email.matches(EMAIL_PATTERN))
                throw new Validator.InvalidValueException("The e-mail address provided is not valid!");
        }
    }
}
