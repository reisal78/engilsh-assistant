package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.ui.Button;

/**
 * Created by Simagin Igor on 03.01.2017.
 */
public interface RegisterForm {



    String getUsername();

    String getPassword();

    String getConfirmPassword();

    String getEmail();

    void setUserName(String value);

    void setPassword(String value);

    void setConfirmPassword(String value);

    void setEmail(String value);

    void setRegisterListener(Button.ClickListener listener);

    void showUsernameError(String error);

    void showPasswordError(String error);

    void showConfirmPasswordError(String error);

    void showEmailError(String error);

    void clearAllErrors();

    boolean validateAllFields();
}
