package org.reisal78.ea.ui.view.vaadin.components;

import com.vaadin.ui.Button;

/**
 * Created by Simagin Igor on 27.12.2016.
 */
public interface LanguagePanel {

    void addLocaleButton(Button button);

    void disableCurrentLocaleButton();
}
