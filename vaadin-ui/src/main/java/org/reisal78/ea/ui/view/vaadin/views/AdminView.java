package org.reisal78.ea.ui.view.vaadin.views;

import com.vaadin.navigator.View;
import com.vaadin.navigator.ViewChangeListener;
import com.vaadin.spring.annotation.SpringView;
import com.vaadin.spring.annotation.UIScope;
import com.vaadin.ui.CustomComponent;
import com.vaadin.ui.Label;
import com.vaadin.ui.VerticalLayout;

import javax.annotation.PostConstruct;

/**
 * Created by Simagin Igor on 18.12.2016.
 */
@SpringView (name = AdminView.VIEW_NAME)
@UIScope
public class AdminView extends CustomComponent implements View {
    public static final String VIEW_NAME = "admin";

    @PostConstruct
    void init () {
        setCompositionRoot(new VerticalLayout(new Label("Admin View")));
    }


    @Override
    public void enter(ViewChangeListener.ViewChangeEvent viewChangeEvent) {

    }
}
