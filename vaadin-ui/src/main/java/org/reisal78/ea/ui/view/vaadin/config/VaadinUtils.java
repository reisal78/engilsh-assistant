package org.reisal78.ea.ui.view.vaadin.config;

import com.vaadin.server.ClientConnector;
import com.vaadin.server.Page;
import com.vaadin.ui.AbstractTextField;
import com.vaadin.ui.JavaScript;

import java.util.Collection;
import java.util.UUID;

/**
 * Created by Simagin Igor on 04.01.2017.
 */
public class VaadinUtils {

    private VaadinUtils() {
    }

    /**
     * Set inputPrompt (or placeholder) via html attribute "placeholder".
     * Also, it fixes PasswordField inputPrompt vaadin old bug.
     */
    public static void setPlaceholder(AbstractTextField textField, String placeholder) {
        textField.setInputPrompt(null);

        String phClass = null;
        String[] styles = textField.getStyleName().split(" ");

        for (String style : styles) {
            if (style.startsWith("x-placeholder-")) {
                phClass = style;
                break;
            }
        }

        if (phClass == null) {
            phClass = "x-placeholder-" + UUID.randomUUID().toString();
            textField.addStyleName(phClass);
        }

        PlaceholderAttachListener attachListener = new PlaceholderAttachListener(phClass, placeholder);

        if (textField.isAttached()) {
            attachListener.attach(null);
        } else {
            Collection<?> listeners = textField.getListeners(ClientConnector.AttachListener.class);

            listeners.stream()
                    .filter(listener -> listener instanceof PlaceholderAttachListener)
                    .forEach(listener -> textField.removeAttachListener((ClientConnector.AttachListener) listener));

            textField.addAttachListener(attachListener);
        }
    }

    private static class PlaceholderAttachListener implements ClientConnector.AttachListener {
        private final String phClass;
        private final String placeholder;

        public PlaceholderAttachListener(String phClass, String placeholder) {
            this.phClass = phClass;
            this.placeholder = placeholder;
        }

        @Override
        public void attach(ClientConnector.AttachEvent event) {
            JavaScript js = Page.getCurrent().getJavaScript();
            js.execute("document.getElementsByClassName('" + phClass + "')[0].placeholder='" + placeholder.replace("'", "\\'") + "'");
        }
    }
}