package org.reisal78.ea.ui.view.vaadin.components;


import com.vaadin.event.ShortcutAction;
import com.vaadin.server.FontAwesome;
import com.vaadin.ui.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * Created by Simagin Igor on 23.12.2016.
 */
@Component
@Scope("prototype")
public class VaadinLoginPanel extends VaadinAbstractComponent implements LoginPanel {

    private TextField usernameField;
    private PasswordField passwordField;
    private Button loginButton;

    @PostConstruct
    public void init() {

        usernameField = new TextField(
                getLocalizedProperty(USERNAME_FIELD_CAPTION)
        );
        usernameField.setIcon(FontAwesome.USER);
        usernameField.setSizeFull();

        passwordField = new PasswordField(
                 getLocalizedProperty(PASSWORD_FIELD_CAPTION)
        );
        passwordField.setIcon(FontAwesome.LOCK);
        passwordField.setSizeFull();

        loginButton = new Button(
                getLocalizedProperty(LOGIN_BUTTON_CAPTION)
        );

        loginButton.setSizeFull();
        loginButton.setClickShortcut(ShortcutAction.KeyCode.ENTER);


        final VerticalLayout root = new VerticalLayout();
        root.setSpacing(true);
        root.setMargin(true);
        root.addComponent(usernameField);
        root.addComponent(passwordField);
        root.addComponent(loginButton);
        root.setComponentAlignment(loginButton, Alignment.MIDDLE_RIGHT);

        Panel loginPanel = new Panel();
        loginPanel.setContent(root);

        setCompositionRoot(loginPanel);
    }

    @Override
    public void setLoginListener(Button.ClickListener listener) {
        loginButton.addClickListener(listener);
    }

    @Override
    public String getUsername() {
        return usernameField.getValue();
    }

    @Override
    public String getPassword() {
        return passwordField.getValue();
    }

    @Override
    public void clearPasswordField() {
        passwordField.setValue("");
    }

}
