package org.reisal78.ea.ui.view.vaadin.components;

/**
 * Created by Simagin Igor on 25.12.2016.
 */
public interface GreetingPanel {
        void setUsername(String username);
}
