package org.reisal78.ea;

import org.reisal78.ea.domain.entity.Locales;
import org.reisal78.ea.domain.entity.User;
import org.reisal78.ea.domain.entity.UserRole;
import org.reisal78.ea.domain.repositories.LocalesRepository;
import org.reisal78.ea.domain.repositories.UserRepository;
import org.reisal78.ea.domain.repositories.UserRoleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.security.SecurityAutoConfiguration;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ResourceBundleMessageSource;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.Locale;

/**
 * Created by Simagin Igor on 16.12.2016.
 */

@SpringBootApplication
@Configuration
@ComponentScan
public class RunApp {


    public static void main(String[] args) {
        SpringApplication.run(RunApp.class);
    }

    @Bean
    public CommandLineRunner init(UserRepository userRepository,
                                  UserRoleRepository roleRepository,
                                  PasswordEncoder passwordEncoder,
                                  LocalesRepository localesRepository) {
        return (args) -> {
            User admin = new User();
            User user = new User();
            admin.setUsername("admin");
            admin.setDisplayedName("ADMIN");
            admin.setPassword(passwordEncoder.encode("admin"));
            admin.setEnabled(true);

            user.setUsername("user");
            user.setDisplayedName("USER");
            user.setPassword(passwordEncoder.encode("user"));
            user.setEnabled(true);

            userRepository.save(admin);
            userRepository.save(user);

            UserRole adminRole = new UserRole();
            adminRole.setRole("ADMIN");
            adminRole.setUserId(admin.getId());
            UserRole userRole = new UserRole();
            userRole.setRole("USER");
            userRole.setUserId(user.getId());

            roleRepository.save(adminRole);
            roleRepository.save(userRole);

            localesRepository.save(new Locales(Locale.ENGLISH));
            localesRepository.save(new Locales(new Locale("Ru")));
            localesRepository.save(new Locales(Locale.FRANCE));


        };
    }

}
